package ru.smochalkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {
}
