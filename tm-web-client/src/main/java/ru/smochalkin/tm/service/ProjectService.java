package ru.smochalkin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository repository;

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<Project> collection) {
        if (collection == null) return;
        for (Project item : collection) {
            save(item);
        }
    }

    @Override
    public Project save(final Project entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Override
    public void create() {
        repository.save(new Project("new project"));
    }

    @Override
    public Project findById(final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void removeById(final String id) {
        repository.deleteById(id);
    }

    @Override
    public void remove(final Project entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}
