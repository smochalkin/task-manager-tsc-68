package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove task by index.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter task index: ");
        @NotNull final Integer index = TerminalUtil.nextInt();
        @NotNull final Result result = taskEndpoint.removeTaskByIndex(sessionService.getSession(), index);
        printResult(result);
    }

}
