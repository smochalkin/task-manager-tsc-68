package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByNameListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove task by name.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter task name: ");
        @NotNull final String taskName = TerminalUtil.nextLine();
        @NotNull final Result result = taskEndpoint.removeTaskByName(sessionService.getSession(), taskName);
        printResult(result);
    }

}
